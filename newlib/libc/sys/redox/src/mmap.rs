use alloc::BTreeMap;
use libc::{c_int, c_void, off_t, size_t};
use spin::{Once, Mutex, MutexGuard};
use syscall;

const MMAP_ANONYMOUS: c_int = 1;

static ANONYMOUS_MAPS: Once<Mutex<BTreeMap<usize, usize>>> = Once::new();

pub fn anonymous_maps() -> MutexGuard<'static, BTreeMap<usize, usize>> {
    ANONYMOUS_MAPS
        .call_once(|| Mutex::new(BTreeMap::new()))
        .lock()
}

libc_fn!(unsafe mmap(addr: *mut c_void, len: size_t, _prot: c_int,
        flags: c_int, fd: c_int, off: off_t) -> Result<*mut c_void> {
    if flags & MMAP_ANONYMOUS == MMAP_ANONYMOUS {
        let fd = syscall::open("memory:", syscall::O_RDWR)?;
        let addr = syscall::fmap(fd as usize, off as usize, len as usize)?;
        anonymous_maps().insert(addr, fd);
        Ok(addr as *mut c_void)
    } else {
        syscall::fmap(fd as usize, off as usize, len as usize)
            .map(|ptr| ptr as *mut c_void)
    }
});
libc_fn!(unsafe munmap(addr: *mut c_void, _len: size_t) -> Result<c_int> {
    syscall::funmap(addr as usize)?;
    {
        let mut maps = anonymous_maps();
        if let Some(fd) = maps.remove(&(addr as usize)) {
            syscall::close(fd)?;
        }
    }
    Ok(0)
});
