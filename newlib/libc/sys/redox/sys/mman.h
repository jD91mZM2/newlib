#include <sys/types.h>

#define MAP_FAILED = NULL;

#define PROT_EXEC 0
#define PROT_READ 0
#define PROT_WRITE 0
#define PROT_NONE 0

#define MAP_SHARED 0
#define MAP_PRIVATE 0
#define MAP_32BIT 0
#define MAP_ANON 1
#define MAP_ANONYMOUS MAP_ANON
#define MAP_DENYWRITE 0
#define MAP_EXECUTABLE 0
#define MAP_FILE 0
#define MAP_FIXED 0
#define MAP_GROWSDOWN 0
#define MAP_HUGETLB 0
#define MAP_HUGE_2MB 0
#define MAP_HUGE_1GB 0
#define MAP_LOCKED 0
#define MAP_NONBLOCK 0
#define MAP_NORESERVE 0
#define MAP_POPULATE 0
#define MAP_STACK 0
#define MAP_UNINITIALIZED 0

void* mmap(void* addr, size_t len, int prot, int flags, int fd, off_t off);
int munmap(void* addr, size_t len);
